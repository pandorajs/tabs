#tabs

[![Build Status](https://api.travis-ci.org/pandorajs/tabs.png?branch=master)](http://travis-ci.org/pandorajs/tabs)
[![Coverage Status](https://coveralls.io/repos/pandorajs/tabs/badge.png?branch=master)](https://coveralls.io/r/pandorajs/tabs?branch=master)

 > tabs, seajs module

##how to demo

1. checkout
1. run `npm install`
1. run `spm install`
1. run `grunt`
1. view files in `/demo`

##how to use

1. run `spm install pandora/tabs`
1. write `require('pandora/tabs/VERSION.NUMBER/tabs')`

##find examples

1. view the source files in '/src'

##history

- 1.0.0 - release
